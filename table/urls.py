from django.urls import path
from .views import *
from . import views

urlpatterns = [   
    path('createline/', views.createLine, name='createLine'),
    path('submitcreate/', views.submitCreate, name='submitCreate'),
    path('editline/<int:lineId>', views.editLine, name='editLine'),
    path('submitedit/<int:lineId>', views.submitEdit, name='submitEdit'),
    path('saveedit/<int:lineId>', views.saveEdit, name='saveEdit'),
    path('submitdelete/<int:lineId>', views.submitDelete, name='submitDelete'),
    path('savedelete/<int:lineId>', views.saveDelete, name='saveDelete'),
    path('togglebutton/<int:buttonId>', views.toggleButton, name='toggleButton'),
    path('', views.table, name='table')
]