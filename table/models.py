from django.db import models



class TableLine(models.Model):
    text = models.TextField(max_length=50)
    active = models.BooleanField()

    def __str__(self):
        return str(self.id)

