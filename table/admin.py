from django.contrib import admin
from .models import TableLine

admin.site.register(TableLine)
