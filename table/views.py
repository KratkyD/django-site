from django.shortcuts import render, redirect
from .models import TableLine
from django.views.decorators.csrf import csrf_exempt

def table(request):
    context = {
        'lines': TableLine.objects.all()
    }
    return render(request, 'table/table.html', context)  

def createLine(request):
    return render(request, 'table/createLine.html')

@csrf_exempt
def submitCreate(request):
    line = TableLine(text=request.POST['text'], active=False)
    line.save()
    return redirect('table')

@csrf_exempt
def editLine(request, lineId):
    context = {
        'lineId' : lineId,
        'text' : TableLine.objects.filter(id=lineId).get().text
    }
    return render(request, 'table/editLine.html', context)

@csrf_exempt
def submitEdit(request, lineId):
    if request.POST['previousText'] == request.POST['text']:
        return redirect('table')
    else:
        context = {
            'lineId' : lineId,
            'inputText' : request.POST['text'],
            'previousText' : request.POST['previousText']
        }
        return render(request, 'table/submitEdit.html', context)
        
@csrf_exempt
def saveEdit(request, lineId):
    if request.method == "POST":
        TableLine.objects.filter(id=lineId).update(text=request.POST['text'])
        return redirect('table')
    else:
        return redirect('../editline/%s' % (lineId))

def submitDelete(request, lineId):
    context = {
        'lineId' : lineId,
        'text' : TableLine.objects.filter(id=lineId).get().text
    }
    return render(request, 'table/submitDelete.html', context)

def saveDelete(request, lineId):
    TableLine.objects.filter(id=lineId).delete()
    return redirect('table')

def toggleButton(request, buttonId):
    button = TableLine.objects.filter(id=buttonId)
    state = TableLine.objects.filter(id=buttonId).get()
    button.update(active = not state.active)
    return redirect('table')

# @csrf_exempt